Introduction
============

This is a small project to demonstrate a simple configuration of Spring Annotation driven beans with values taken from a
properties file, found on the classpath.


Pre-requisites
--------------

Maven