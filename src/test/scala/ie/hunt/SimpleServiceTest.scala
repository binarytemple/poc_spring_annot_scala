package ie.hunt

import org.junit.runner.RunWith
import org.junit.{Assert, Test}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(classOf[SpringJUnit4ClassRunner])
@ContextConfiguration(locations = Array("classpath*:/applicationContext.xml"))
class SimpleServiceTest {

  @Autowired
  var underTest: SimpleService = null

  @Test
  def testValueWasSet() {
    try {
      underTest.runOperation()
    }
    catch {
      case _ => Assert.fail("operation should not throw exception")
    }
  }
}