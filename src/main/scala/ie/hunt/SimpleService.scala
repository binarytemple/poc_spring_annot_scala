package ie.hunt

import org.springframework.beans.factory.annotation.{Required, Value}
import org.springframework.jmx.export.annotation.ManagedResource
import org.springframework.stereotype.Component
import reflect.{BooleanBeanProperty, BeanProperty}


@ManagedResource("example:name=SimpleService")
@Component
class SimpleService {

  @Value("${testvalue}")
  @Required
  @BooleanBeanProperty
  var testValue: Boolean = false

  def runOperation() {
    if (testValue == false)
      throw new UnsupportedOperationException
  }
}